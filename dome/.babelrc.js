const plugins = [
  [
    "@babel/plugin-proposal-decorators",
    {
      "legacy": true,
    },
  ],
];

const configs = {
  presets: ["react-app"],
  plugins,
};
if (process.env.NODE_ENV === "production") {
  plugins.push("transform-remove-console");
}
module.exports = configs;
