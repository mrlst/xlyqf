import React, { Component } from "react";
import RouterView from "../router/RouterView";
import { NavLink } from "react-router-dom";
class Index extends Component {
  render() {
    let { config, navlink } = this.props;

    return (
      <div>
        <div>
          {navlink && navlink.length > 0
            ? navlink.map((item, index) => {
                return (
                  <NavLink key={index} to={item.path} activeStyle={{ color: "pink" }}>
                    {item.name}
                  </NavLink>
                );
              })
            : ""}
        </div>
        <div>{<RouterView config={config} />}</div>
      </div>
    );
  }
}

export default Index;
