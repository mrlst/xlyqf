import Loadable from "react-loadable";

function Loading() {
  return <div>Loading....</div>;
}
const Index = Loadable({
  loader: () => import("../page/Index"),
  loading: Loading,
});
const About = Loadable({
  loader: () => import("../page/home/About"),
  loading: Loading,
});
const Archive = Loadable({
  loader: () => import("../page/home/Archive"),
  loading: Loading,
});
const Article = Loadable({
  loader: () => import("../page/home/Article"),
  loading: Loading,
});
const Booklet = Loadable({
  loader: () => import("../page/home/Booklet"),
  loading: Loading,
});
const Message = Loadable({
  loader: () => import("../page/home/Message"),
  loading: Loading,
});
const All = Loadable({
  loader: () => import("../page/home/all/All"),
  loading: Loading,
});
const Front = Loadable({
  loader: () => import("../page/home/all/Front"),
  loading: Loading,
});
const LeetCode = Loadable({
  loader: () => import("../page/home/all/LeetCode"),
  loading: Loading,
});
const Linux = Loadable({
  loader: () => import("../page/home/all/Linux"),
  loading: Loading,
});
const News = Loadable({
  loader: () => import("../page/home/all/News"),
  loading: Loading,
});
const Read = Loadable({
  loader: () => import("../page/home/all/Read"),
  loading: Loading,
});
const Rear = Loadable({
  loader: () => import("../page/home/all/Rear"),
  loading: Loading,
});

let config = [
  {
    path: "/index",
    component: Index,
    children: [
      {
        path: "/index/home/article",
        component: Article,
        name: "文章",
        children: [
          {
            path: "/index/home/article/all",
            component: All,
            name: "所有",
          },
          {
            path: "/index/home/article/front",
            component: Front,
            name: "前端",
          },
          {
            path: "/index/home/article/rear",
            component: Rear,
            name: "后端",
          },
          {
            path: "/index/home/article/read",
            component: Read,
            name: "阅读",
          },
          {
            path: "/index/home/article/linux",
            component: Linux,
            name: "Linux",
          },
          {
            path: "/index/home/article/leetCode",
            component: LeetCode,
            name: "LeetCode",
          },
          {
            path: "/index/home/article/news",
            component: News,
            name: "要闻",
          },
          {
            from: "/index/home/article",
            to: "/index/home/article/all",
          },
        ],
      },
      {
        path: "/index/home/archive",
        component: Archive,
        name: "归档",
      },
      {
        path: "/index/home/booklet",
        component: Booklet,
        name: "知识小册",
      },
      {
        path: "/index/home/message",
        component: Message,
        name: "留言板",
      },
      {
        path: "/index/home/about",
        component: About,
        name: "关于",
      },
      {
        from: "/index/home",
        to: "/index/home/article",
      },
    ],
  },
  {
    from: "/",
    to: "/index/home",
  },
];

export default config;
