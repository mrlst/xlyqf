import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
function RouterView(props) {
  let { config } = props;

  let route = config.filter((itme) => !itme.to);
  let redirect = config.filter((itme) => itme.to);
  return (
    <Switch>
      {route && route.length > 0
        ? route.map((itme, index) => {
            return (
              <Route
                path={itme.path}
                key={index}
                render={(renderProps) => {
                  let Com = itme.component;
                  if (itme.isRequire && !localStorage.user) {
                    return <Redirect to="/login" key={index}></Redirect>;
                  }
                  if (itme.children) {
                    return (
                      <Com
                        config={itme.children}
                        {...renderProps}
                        navlink={itme.children.filter((itme) => !itme.to)}
                      ></Com>
                    );
                  }
                  return <Com {...renderProps}></Com>;
                }}
              ></Route>
            );
          })
        : null}
      {redirect &&
        redirect.map((itme) => <Redirect to={itme.to} key={itme.to} from={itme.from}></Redirect>)}
    </Switch>
  );
}

export default RouterView;
