import React, { Component } from "react";
import RouterView from "./router/RouterView";
import config from "./router/config";
class App extends Component {
  render() {
    return (
      <div>
        <RouterView config={config} />
      </div>
    );
  }
}

export default App;
